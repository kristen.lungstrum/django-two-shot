# Generated by Django 4.1.5 on 2023-01-21 08:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_alter_receipt_date"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="number",
            field=models.TextField(max_length=20),
        ),
    ]
